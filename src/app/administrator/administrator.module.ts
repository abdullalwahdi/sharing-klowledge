import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule, Routes } from '@angular/router';
import { SettingsComponent } from './settings/settings.component';
import { ProjectComponent } from './project/project.component';
import { HomeComponent } from './home/home.component';
import { ProjectaddComponent } from './projectadd/projectadd.component';
import { MatDialogModule } from '@angular/material/dialog';
import { PopupComponent } from './popup/popup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field'; 


const routes: Routes = [
  {
    path:'',component:MainComponent,
    children:[
      {
        path:'',
        redirectTo:'/admin/home',
        pathMatch:'full'
      },
      {
        path:'home',component:HomeComponent
      },
      {
        path:'project',component:ProjectComponent
      },
      {
        path:'settings',component:SettingsComponent
      }
    ]
  }
]

@NgModule({
  declarations: [
    MainComponent,
    SettingsComponent,
    ProjectComponent,
    ProjectaddComponent,
    PopupComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    RouterModule.forChild(routes)
  ]
})
export class AdministratorModule { }
