import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(
    private httpClient: HttpClient
  ) { }

  httpOptions: any
  url = 'http://localhost:3000/api/master/actor'

  getDataProjet() {
    return this.httpClient.get(this.url, this.httpOptions)
  }
}
