import { Component, OnInit } from '@angular/core';
import { ProjectService } from './project.service';
import { MatDialog } from '@angular/material/dialog';
import { PopupComponent } from '../popup/popup.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  dataProject : any
  columTable = ['actor_id', 'first_name', 'last_name', 'Update', 'Delete']
  titlePage = "ACTOR"
  constructor(
    private projecService : ProjectService,private dialog:MatDialog
  ) {}

  ngOnInit(): void {
    this.getDataProject()
  }
  getDataProject(){
    this.projecService.getDataProjet().subscribe(res =>{
      this.dataProject = res
      console.log(res)

    })

  }

  Openpopup(){
    var _popup=this.dialog.open(PopupComponent,{
      width:'20%',
      data:{
        title:'Tambahakan User'
      }
    });
    _popup.afterClosed().subscribe(item=>{
      console.log(item)
    })
  }

}
